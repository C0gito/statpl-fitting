import numpy as np
import os
import linecache
import subprocess

#================================================================================
#DEFINE PATHS
#================================================================================
pydir=os.getcwd()
statplpath='/home/axel/NGC628/Luminosity_function/Stat/statplsource/'

#================================================================================
#DEFINE FUNCTIONS
#================================================================================
#Function for writing statpl input file 
def writestat(filename):
	outputfile='inputstatpl'
	file=open(outputfile,'w')
	a='{}		! name of data file massspec.dat'.format(filename)
	b='-1		! number of bins, -1 for auto'
	c='0		! Beg'
	d='0		! Beg recursive'
	e='0		! Baxter'
	f='0		! Const bin LR'
	g='0		! Var bin chi'
	h='0		! ML'
	i='1		! Mod  ML'
	j='0		! CCD'
	k='1		! calculate standard deviation of estimators and power of tests'
	l='1000		! Monte-Carlo sampling size'
	m='0.05		! Significance level'
	n='0		! make plots'

	newline=a+'\n'+b+'\n'+c+'\n'+d+'\n'+e+'\n'+f+'\n'+g+'\n'+h+'\n'+i+'\n'+j+'\n'+k+'\n'+l+'\n'+m+'\n'+n+'\n'
	file.write(newline)
	file.close()

#Function for writing a file w. data for the statpl fitting
#Depends sensitively upon the different
def writedata(filename,data,uplim = [float('NaN')],lowlim = [float('NaN')]):
	if uplim and lowlim:
		ident=np.where((data<uplim)&(data>lowlim))
	elif uplim:
		ident=np.where(data<uplim)
	elif lowlim:
		ident=np.where(data<lowlim)
	data_ident=data[ident]
	data_ident=data_ident[::-1]

	file=open(filename,'w')
	x=10**((data_ident)/(-2.5))
	for a in np.arange(len(x)):
		c=str(x[a])
		newline=c+'\n' 		#b+'\t'+c+'\n'
		file.write(newline)
	file.close()

#Function for performing the fitting given a datafile
def statplfit(datafilename,uplims,lowlims=[float('NaN')],time=120):
	result_slope=[]
	result_slope_errs=[]

	# Move datafiles to statpl directory
	os.system('cp ' + datafilename + ' ' + statplpath + datafilename)

	#Move statplinputfile to statpl directory
	os.system('cp inputstatpl ' + statplpath + 'inputstatpl')

	# Run statpl
	cmd=statplpath+'statpl < inputstatpl'

	p=subprocess.Popen(cmd,shell=True,stdout=subprocess.DEVNULL,stderr=subprocess.DEVNULL)
	try:
		p.wait(timeout=time)
	except subprocess.TimeoutExpired:
		print('\t Statpl timed out! Insufficient data to perform fit')
		p.terminate()

	
	#s.communicate()
	#Remove datafile
	os.system('rm '+statplpath+inputfile)

	#Move and rename resultsfile
	resultsfile=inputs[a]+'_results.txt'
	os.system('mv statmfout.txt '+pydir+'/'+fltr[Filter]+'/'+resultsfile)

	#Remove statplfile
	os.system('rm inputstatpl')

	#Remove inputfile
	os.system('rm '+inputfile)


	#Extract the results.
	#Best fit is on line 51
	temp1=(linecache.getline(pydir+'/'+fltr[Filter]+'/'+resultsfile, 51).strip())
	
	#Error of best fit is on line 53
	temp2=(linecache.getline(pydir+'/'+fltr[Filter]+'/'+resultsfile, 53).strip())
	

	#Convert result string to numbers
	if temp1=='':
		result_slope.append(float('NaN'))
		result_slope_errs.append(float('NaN'))
		success = 0
	else:
		result_slope.append(float(temp1))
		result_slope_errs.append(float(temp2))
		success = 1

	return success,result_slope,result_slope_errs


#DOWN HERE BE STUFF THAT IS UNUSED
'''
for a in np.arange(len(filename)):
	

	#Create inputfile
	inputfile=inputs[a]+'.dat'
	writestat(inputfile)

	#Create datafile
	
	if lowlims[0]==lowlims[0]:
		uplim=uplims[a]
		lowlim=lowlims[a]
		writedata(inputfile,uplim,lowlim)
	else:
		uplim=uplims[a]
		writedata(inputfile,uplim)




print('Loading data')
data=np.loadtxt('ngc628c_cluster_legus_avgapcor_PadAGB_MWext_28Jun15.tab',usecols=(5,7,9,11,13,30,32,33))
mags=data[:,0:5]
chi2=data[:,5]
Nflt=data[:,6]
Class=data[:,7]
'''




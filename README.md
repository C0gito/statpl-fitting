# README #

This repo is for the stat-pl related fitting for the LEGUS cluster luminosity functions.

##File descriptions##
**Statpl-folder**

  Contains the source file for the stat-pl fitting program

**stat.py**
  
  Contains a number of functions for performing the fitting and extracting the data from the finished result.

  Descriptions of the functions in this file can be found in the *wiki*. It should be noted that the figures are somewhat untested, due to modifications I made in order to make them more useful and understandable. However the main features are working

**inputstatpl**

  Example input file for statpl:

```
mag20-22.dat	! name of data file massspec.dat
-1		! number of bins, -1 for auto
0		! Beg
0		! Beg recursive
0		! Baxter
0		! Const bin LR
0		! Var bin chi
0		! ML
1		! Mod  ML
0		! CCD
1		! calculate standard deviation of estimators and power of tests
1000		! Monte-Carlo sampling size
0.05		! Significance level
0		! make plots
```

From the line Beg to Mod ML the lines represent different fitting algorithms. If I've understood it correctly Mod-ML (Modified Maximum likelyhood) is the one we care about since it provides the best estimate.